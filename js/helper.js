'use strict';

import { showError } from './functions.js';

/** Wait until document is ready
 * @function ready
 * @param {*} callback 
 * @return {*} 
 */
function ready(callback) {
    if (document.readyState === 'complete') {
        callback();
    } else {
        document.addEventListener('DOMContentLoaded', callback);
    }
}

/** Load response in JSON format
 * @function loadJSON
 * @param {string} url 
 * @param {string} method 
 * @param {string} data 
 */
function loadJSON(url,method,data){
    method = method || 'GET';
    data = data || '';
    
    return new Promise((resolve,reject) => {
        const req = new XMLHttpRequest();
        req.open(method, url);
        
        if(method === 'POST' && data !== undefined){
            req.setRequestHeader("Content-Type", "application/json");
            req.send(JSON.stringify(data)); 
        }
        else{
           req.send(); 
        }
        req.addEventListener('load', e => {
           if(req.status === 200){
               if(req.getResponseHeader('Content-Type').indexOf('application/json') > -1){
                    resolve(JSON.parse(req.responseText));
               }
               else{
                   resolve(req.responseText);
               }
           }
           else{
               showError(req.status, req.statusText, req.response);
               reject(e);
           }
        });
        
        req.addEventListener('load', e => reject(e));
    });
}

export {ready, loadJSON};