'use strict';

import { config } from '../config.js';
import { loadJSON } from '../helper.js';
import { genres } from '../functions.js';
/**
 * Movie Class
 */
export class Movie{
    constructor(data){
        this.poster_path = data.poster_path;
        this.adult = data.adult;
        this.overview = data.overview;
        this.release_date = data.release_date;
        this.genre_ids = data.genre_ids || [];
        this.id = data.id;
        this.original_title = data.original_title;
        this.original_language = data.original_language;
        this.title = data.title;
        this.backdrop_path = data.backdrop_path;
        this.popularity = data.popularity;
        this.vote_count = data.vote_count;
        this.video = data.video;
        this.vote_average = data.vote_average;
    }
    /** 
     * Return list of genres
     */
    getGenres(){
        return this.genre_ids.map(item => { return genres[item] }).join(', ')
    }
    /**
     * Get Poster image
     */
    getPoster(){
        return this.poster_path ? `<img class="poster" src="${config.base_url_img}/w185_and_h278_bestv2/${this.poster_path}" alt="${this.title}">` : 'No poster'; 
    }
    /**
     * Generate link to movie to TMDB site 
     */
    getLink(){
        return `<a href='${config.original_url}/${this.id}' target='_blank'>${this.title}</a>`
    }
    /**
     * Return release year
     */
    getYear(){
        return new Date(this.release_date).getFullYear();
    }
    /**
     * Get overview information
     */
    getDetails(){
        return this.overview ? this.overview : `No overview present.`
    }
    /**
     * Print card with main movie information
     */
    printShortInfo(){
        return `
            <div class='row card'>
                 <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 left-side">
                    ${this.getPoster()}
                 </div>

                 <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 right-side">
                    <div class='row mainInfo'>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 titleColumn">
                            ${this.getLink()} (<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> ${this.getYear()})
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ratingColumn">
                            Rating: ${this.vote_average} <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 genresColumn">
                            Genre: ${this.getGenres()}
                        </div>
                    </div>
                    <div class='row mainDetails'>
                        Details: ${this.getDetails()}
                    </div>
                 </div>
            </div>
            
        `;
    }
}
