'use strict';

import { config } from './config.js';
import { Movie } from './models/movie.js';
import { loadJSON } from './helper.js';

let searchInput;
let searchButton;
let outputContainer;
let totalContainer;
let pagerPrev;
let pagerNext;
let errorContainer;
let genres = [];

/** Initialize buttons and add listeners
 * @function init
 */
function init(){
    searchInput = document.querySelector('input.searchInput');
    searchButton = document.querySelector('button.searchButton');
    outputContainer = document.querySelector('div.outputContainer');
    pagerPrev = document.querySelector('ul.pager.prev');
    pagerNext = document.querySelector('ul.pager.next');
    errorContainer = document.querySelector('div.alert');
    totalContainer = document.querySelector('span.total');
    searchInput.focus();
    loadGenres();
    
    searchInput.addEventListener('keyup',event => {
        if (event.keyCode === 13) {
            searchFilm();
        } 
    });
    searchButton.addEventListener('click', searchFilm);
}

/** Load all genres from local storage if exist otherwise loads from API
 * @function loadGenres
 */
function loadGenres(){
    let genresFromLS = JSON.parse(localStorage.getItem('genres'));
    if(!genresFromLS){
        let url = `${config.base_url}/genre/movie/list?api_key=${config.api_key}`;
        loadJSON(url).then( result => {
            result.genres.forEach(item => { genres[item.id] = item.name });
            let genresLS = JSON.stringify(result.genres);
            localStorage.setItem('genres',genresLS);
        });
    }
    else{
        genresFromLS.forEach(item => { genres[item.id] = item.name });
    }
}

/** Search movie
 * @function searchFilm
 * @param {number} page 
 */
function searchFilm(page = 1){
    console.log(`page ${page}`);
    let query = searchInput.value;
    if(query.length < 1){
        return;
    }
    query = encodeURI(query);
    let url = `${config.base_url}/search/movie?api_key=${config.api_key}&query=${query}`;
    if(page !== 1 && !isNaN(page)){ 
        url = `${config.base_url}/search/movie?api_key=${config.api_key}&query=${query}&page=${page}`;
    }
    loadJSON(url).then( result => {
        reflow(result);
        
    });
}

/** Print search results on page
 * @function reflow
 * @param {object} data 
 */
function reflow(data){
    makePager(data.page, data.total_pages, data.total_results);
    outputContainer.innerHTML = '';
    let docFragment = '';
    if(data.total_results > 0){
        for(let movie of data.results){
            let film = new Movie(movie);
            docFragment += film.printShortInfo();
        }
    }
    else{
        docFragment += `<span class="info">Nothing Found</span>`;
    }
    outputContainer.innerHTML += docFragment;    
}

/** Generate pager for navigating
 * @function makePager
 * @param {number} page 
 * @param {number} totalPages 
 * @param {number} totalResults 
 */
function makePager(page, totalPages, totalResults){
    pagerNext.innerHTML = '';
    pagerPrev.innerHTML = '';
    totalContainer.innerHTML = '';
    if(totalResults !== 0){
        if((page - 1) >= 1){
            pagerPrev.innerHTML = `<li><a href="#" data-page="${page-1}" class='page'>Prev</a></li>`;
        }
        if ((page + 1) <= totalPages){
            pagerNext.innerHTML = `<li><a href="#" data-page="${page+1}" class='page'>Next</a></li>`;
        }
        totalContainer.innerHTML = `Showing ${page} of ${totalPages} (${totalResults} movies)`;

        document.querySelectorAll('a.page').forEach(function(element) {
            element.addEventListener('click', function(e){ 
                searchFilm(e.target.getAttribute('data-page')); 
            });
        }, this);
    }
}

/** Display error message for 10 seconds
 * @function showError
 * @param {*} errorCode 
 * @param {string} errorMessage 
 * @param {string} response
 */
function showError(errorCode, errorMessage, response){
    let message = JSON.parse(response);
    
    errorContainer.querySelector('span.errorMsg').innerHTML = `Error! Code: ${errorCode}<br>Message: ${errorMessage}<br>Response from API:${message.status_message}`;
    errorContainer.style.visibility = 'visible'; 
    setTimeout(()=>{
        errorContainer.style.visibility = 'hidden';
    }, 10000);
}

export {init, searchFilm, reflow, makePager, genres, showError};