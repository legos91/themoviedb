# README #

This README would normally document whatever steps are necessary to get this application up and running.

### What is this repository for? ###

Test task from Sii. Project uses TheMovieDB Api. 

### How do I get set up? ###

Before first run you should execute command "npm init" inside project folder. This allows add all necessary plugins to this project. 

To RUN the app you should execute "npm run start" command. Browser will automatically open page with application.
